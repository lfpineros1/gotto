package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func getKeyboardInput() string {
	reader := bufio.NewReader(os.Stdin)
	text, _ := reader.ReadString('\n')
	return strings.TrimSpace(text)
}

func getWord() string {
	return "cakes" // placeholder value
}

func checkGuess(guess string, word string) (success bool, output string) {
	count := 0

	for _, v := range guess {
		if strings.Contains(word, string(v)) {
			count++
		}
	}

	if count == 5 && guess == word {
		success = true
		output = "You got it!"
	} else {
		output = fmt.Sprintf("Wrong answer, loser! Only %v letters matched!", count)
	}

	return
}

func main() {
	success, output := false, ""
	word := getWord()

	for !success {
		fmt.Print("Please guess a 5 letter word: ")
		guess := getKeyboardInput()

		fmt.Println(len(guess))
		if len(guess) == 5 {
			success, output = checkGuess(guess, word)
			fmt.Println(output)
		} else {
			fmt.Println("Word must be 5 letters")
		}
	}
}
