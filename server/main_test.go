package main

import (
	"testing"
)

func TestCheckGuess(t *testing.T) {
	word := "cakes"
	tables := []struct {
		guess           string
		expectedSuccess bool
		expectedOutput  string
	}{
		{"abcdefg", false, "Wrong answer, loser! Only 3 letters matched!"},
		{"abcde", false, "Wrong answer, loser! Only 3 letters matched!"},
		{"", false, "Wrong answer, loser! Only 0 letters matched!"},
		{"cakes", true, "You got it!"},
		{"scaek", false, "Wrong answer, loser! Only 5 letters matched!"},
	}

	for _, table := range tables {
		actualSuccess, actualOutput := checkGuess(table.guess, word)
		if actualSuccess != table.expectedSuccess {
			t.Errorf("Output incorrect, got %v, want %v", actualSuccess, table.expectedSuccess)
		}
		if actualOutput != table.expectedOutput {
			t.Errorf("Output incorrect, got %v, want %v", actualOutput, table.expectedOutput)
		}
	}
}
