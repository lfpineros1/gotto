package main

import (
	"encoding/json"
	"math/rand"
	"net/http"
	"time"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

type FiveLetterWord struct {
	Word string `json:"word"`
}

func HandleRequest() (events.APIGatewayProxyResponse, error) {
	word := getWord()

	js, err := json.Marshal(word)
	if err != nil {
		serverError(err)
	}

	return events.APIGatewayProxyResponse{
		StatusCode: http.StatusOK,
		Headers:    map[string]string{"Content-Type": "application/json"},
		Body:       string(js),
	}, nil
}

func getWord() FiveLetterWord {
	word := words[getRandomIndex()]

	return FiveLetterWord{
		Word: word,
	}
}

func getRandomIndex() int {
	rand.Seed(time.Now().UnixNano())
	return rand.Intn(len(words))
}

func serverError(err error) (events.APIGatewayProxyResponse, error) {
	return events.APIGatewayProxyResponse{
		StatusCode: http.StatusInternalServerError,
		Body:       http.StatusText(http.StatusInternalServerError),
	}, nil
}

func main() {
	lambda.Start(HandleRequest)
}
